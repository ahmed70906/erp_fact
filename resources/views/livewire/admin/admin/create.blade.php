<div>

    <form wire:submit.live="save">
    <div class="container row">
            <div class="form-group col-md-6">
                <label for="exampleInputEmail1">الاسم</label>
                <input type="text" class="form-control" wire:model="name" placeholder="Enter name">
                <div class="text-red">@error('name') {{ $message }} @enderror</div>
            </div>
                       <div class="form-group col-md-6">
                <label for="exampleInputEmail1">البريد</label>
                <input type="email" class="form-control" wire:model="email"  placeholder="Enter email">
                           <div class="text-red">@error('email') {{ $message }} @enderror</div>

                       </div>
                       <div class="form-group col-md-6">
                <label for="exampleInputEmail1">موبايل</label>
                <input type="number" class="form-control" wire:model="phone"  placeholder="Enter phone">
                           <div class="text-red">@error('phone') {{ $message }} @enderror</div>

                       </div>
                       <div class="form-group col-md-6">
                <label for="exampleInputEmail1">العنوان</label>
                <input type="text" class="form-control" wire:model="title"  placeholder="Enter title">
                           <div class="text-red">@error('title') {{ $message }} @enderror</div>

                       </div>
        <div class="form-group col-md-6">
                <label for="exampleInputEmail1">الرقم السري</label>
                <input type="password" class="form-control" wire:model="password" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter password">
                           <div class="text-red">@error('password') {{ $message }} @enderror</div>
                       </div>
        <div class="form-group">
            <label for="exampleInputEmail1">نشط</label>
            <input type="checkbox" class="form-control" wire:model="active">
            <div class="text-red">@error('title') {{ $message }} @enderror</div>

        </div>
            <button type="submit" class="btn btn-primary col-md-12">Submit</button>
    </div>
    </form>
</div>
