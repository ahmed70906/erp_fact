<div>
    <section class="content">
        <div class="container-fluid">
            <!-- Small boxes (Stat box) -->
            <form>
                <section>
                    <div class="container py-5">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card mb-4">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <p class="mb-0">الاسم</p>
                                            </div>
                                            <div class="col-sm-9">
                                                <input class="form-control" name="name" type="text" placeholder="الاسم" value="{{$detail->name}}">
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <p class="mb-0">البريد الالكتروني</p>
                                            </div>
                                            <div class="col-sm-9">
                                                <input class="form-control" type="text" placeholder="البريد الالكتروني" value="{{$detail->email}}">
                                                </p>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <p class="mb-0">موبايل</p>
                                            </div>
                                            <div class="col-sm-9">
                                                <input class="form-control" name="phone" type="text" placeholder="موبايل" value="{{$detail->phone}}">
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <p class="mb-0">العنوان</p>
                                            </div>
                                            <div class="col-sm-9">
                                                <p class="text-muted mb-0">
                                                    <input class="form-control" name="title" type="text" placeholder="العنوان" value="{{$detail->title}}">
                                                </p>
                                            </div>
                                            {{--                                        <button class="btn btn-danger">تعديل</button>--}}
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <p class="mb-0">الصورة</p>
                                            </div>
                                            <div class="col-sm-9">
                                                <p class="text-muted mb-0">
                                                    <input class="form-control" name="image" type="text" placeholder="الصورة" value="{{$detail->image}}">
                                                </p>
                                            </div>
                                            {{--                                        <button class="btn btn-danger">تعديل</button>--}}
                                        </div> <hr>
                                        {{--                                        <div class="row">--}}
                                        {{--                                            <div class="col-sm-3">--}}
                                        {{--                                                <p class="mb-0">الرقم السري الحالي</p>--}}
                                        {{--                                            </div>--}}
                                        {{--                                            <div class="col-sm-9">--}}
                                        {{--                                                <p class="text-muted mb-0">--}}
                                        {{--                                                    <input class="form-control" name="old_password" type="text" placeholder="الرقم السري القديم" value="{{$detail->password}}">--}}
                                        {{--                                                </p>--}}
                                        {{--                                            </div>--}}
                                        {{--                                            --}}{{--                                        <button class="btn btn-danger">تعديل</button>--}}
                                        {{--                                        </div>--}}
                                        {{--                                        <hr>--}}
                                        {{--                                        <div class="row">--}}
                                        {{--                                            <div class="col-sm-3">--}}
                                        {{--                                                <p class="mb-0">الرقم السري الجديد</p>--}}
                                        {{--                                            </div>--}}
                                        {{--                                            <div class="col-sm-9">--}}
                                        {{--                                                <p class="text-muted mb-0">--}}
                                        {{--                                                    <input class="form-control" name="new_password" type="text" placeholder="الرقم السري الجديد" value="{{$detail->title}}">--}}
                                        {{--                                                </p>--}}
                                        {{--                                            </div>--}}
                                        {{--                                            --}}{{--                                        <button class="btn btn-danger">تعديل</button>--}}
                                        {{--                                        </div>--}}
                                        {{--                                        <hr>--}}
                                        {{--                                        <div class="row">--}}
                                        {{--                                            <div class="col-sm-3">--}}
                                        {{--                                                <p class="mb-0">تأكيد الرقم السري </p>--}}
                                        {{--                                            </div>--}}
                                        {{--                                            <div class="col-sm-9">--}}
                                        {{--                                                <p class="text-muted mb-0">--}}
                                        {{--                                                    <input class="form-control" name="new_password" type="text" placeholder="الرقم السري الجديد" value="{{$detail->title}}">--}}
                                        {{--                                                </p>--}}
                                        {{--                                            </div>--}}
                                        {{--                                            --}}{{--                                        <button class="btn btn-danger">تعديل</button>--}}
                                        {{--                                        </div>--}}
                                        <hr>
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <p class="mb-0">العمليات</p>
                                            </div>
                                            <div class="col-sm-9">
                                                <p class="text-muted mb-0">
                                                    <a class="btn btn-danger" href="{{route('admin.details.edit' , \Illuminate\Support\Facades\Auth::guard('admin')->user()->id)}}">تعديل</a>
                                                </p>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                                {{--                            <div class="row">--}}
                                {{--                                <div class="col-md-6">--}}
                                {{--                                    <div class="card mb-4 mb-md-0">--}}
                                {{--                                        <div class="card-body">--}}
                                {{--                                            <p class="mb-4"><span class="text-primary font-italic me-1">assigment</span> Project Status--}}
                                {{--                                            </p>--}}
                                {{--                                            <p class="mb-1" style="font-size: .77rem;">Web Design</p>--}}
                                {{--                                            <div class="progress rounded" style="height: 5px;">--}}
                                {{--                                                <div class="progress-bar" role="progressbar" style="width: 80%" aria-valuenow="80"--}}
                                {{--                                                     aria-valuemin="0" aria-valuemax="100"></div>--}}
                                {{--                                            </div>--}}
                                {{--                                            <p class="mt-4 mb-1" style="font-size: .77rem;">Website Markup</p>--}}
                                {{--                                            <div class="progress rounded" style="height: 5px;">--}}
                                {{--                                                <div class="progress-bar" role="progressbar" style="width: 72%" aria-valuenow="72"--}}
                                {{--                                                     aria-valuemin="0" aria-valuemax="100"></div>--}}
                                {{--                                            </div>--}}
                                {{--                                            <p class="mt-4 mb-1" style="font-size: .77rem;">One Page</p>--}}
                                {{--                                            <div class="progress rounded" style="height: 5px;">--}}
                                {{--                                                <div class="progress-bar" role="progressbar" style="width: 89%" aria-valuenow="89"--}}
                                {{--                                                     aria-valuemin="0" aria-valuemax="100"></div>--}}
                                {{--                                            </div>--}}
                                {{--                                            <p class="mt-4 mb-1" style="font-size: .77rem;">Mobile Template</p>--}}
                                {{--                                            <div class="progress rounded" style="height: 5px;">--}}
                                {{--                                                <div class="progress-bar" role="progressbar" style="width: 55%" aria-valuenow="55"--}}
                                {{--                                                     aria-valuemin="0" aria-valuemax="100"></div>--}}
                                {{--                                            </div>--}}
                                {{--                                            <p class="mt-4 mb-1" style="font-size: .77rem;">Backend API</p>--}}
                                {{--                                            <div class="progress rounded mb-2" style="height: 5px;">--}}
                                {{--                                                <div class="progress-bar" role="progressbar" style="width: 66%" aria-valuenow="66"--}}
                                {{--                                                     aria-valuemin="0" aria-valuemax="100"></div>--}}
                                {{--                                            </div>--}}
                                {{--                                        </div>--}}
                                {{--                                    </div>--}}
                                {{--                                </div>--}}
                                {{--                                <div class="col-md-6">--}}
                                {{--                                    <div class="card mb-4 mb-md-0">--}}
                                {{--                                        <div class="card-body">--}}
                                {{--                                            <p class="mb-4"><span class="text-primary font-italic me-1">assigment</span> Project Status--}}
                                {{--                                            </p>--}}
                                {{--                                            <p class="mb-1" style="font-size: .77rem;">Web Design</p>--}}
                                {{--                                            <div class="progress rounded" style="height: 5px;">--}}
                                {{--                                                <div class="progress-bar" role="progressbar" style="width: 80%" aria-valuenow="80"--}}
                                {{--                                                     aria-valuemin="0" aria-valuemax="100"></div>--}}
                                {{--                                            </div>--}}
                                {{--                                            <p class="mt-4 mb-1" style="font-size: .77rem;">Website Markup</p>--}}
                                {{--                                            <div class="progress rounded" style="height: 5px;">--}}
                                {{--                                                <div class="progress-bar" role="progressbar" style="width: 72%" aria-valuenow="72"--}}
                                {{--                                                     aria-valuemin="0" aria-valuemax="100"></div>--}}
                                {{--                                            </div>--}}
                                {{--                                            <p class="mt-4 mb-1" style="font-size: .77rem;">One Page</p>--}}
                                {{--                                            <div class="progress rounded" style="height: 5px;">--}}
                                {{--                                                <div class="progress-bar" role="progressbar" style="width: 89%" aria-valuenow="89"--}}
                                {{--                                                     aria-valuemin="0" aria-valuemax="100"></div>--}}
                                {{--                                            </div>--}}
                                {{--                                            <p class="mt-4 mb-1" style="font-size: .77rem;">Mobile Template</p>--}}
                                {{--                                            <div class="progress rounded" style="height: 5px;">--}}
                                {{--                                                <div class="progress-bar" role="progressbar" style="width: 55%" aria-valuenow="55"--}}
                                {{--                                                     aria-valuemin="0" aria-valuemax="100"></div>--}}
                                {{--                                            </div>--}}
                                {{--                                            <p class="mt-4 mb-1" style="font-size: .77rem;">Backend API</p>--}}
                                {{--                                            <div class="progress rounded mb-2" style="height: 5px;">--}}
                                {{--                                                <div class="progress-bar" role="progressbar" style="width: 66%" aria-valuenow="66"--}}
                                {{--                                                     aria-valuemin="0" aria-valuemax="100"></div>--}}
                                {{--                                            </div>--}}
                                {{--                                        </div>--}}
                                {{--                                    </div>--}}
                                {{--                                </div>--}}
                                {{--                            </div>--}}
                            </div>
                        </div>
                    </div>
                </section>
            </form>

            <!-- /.row (main row) -->
        </div><!-- /.container-fluid -->
    </section>
</div>
