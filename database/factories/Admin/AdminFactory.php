<?php

namespace Database\Factories\Admin;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\admin\Admin>
 */
class AdminFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'name' => $this->faker->name,
            'phone' => $this->faker->phoneNumber,
            'title' => $this->faker->title,
            'type' => 'admin',
            'email' => $this->faker->email,
            'image' => $this->faker->image,
            'state' => 0,
            'password' => bcrypt('123456789'),
        ];
    }
}
