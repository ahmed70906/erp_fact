<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreAdminDetailRequest;
use App\Http\Requests\UpdateAdminDetailRequest;
use App\Models\admin\Admin;
use App\Models\admin\AdminDetail;
use Illuminate\Support\Facades\Auth;

class AdminDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
//        return view('admin.profile.details');
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreAdminDetailRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(AdminDetail $adminDetail)
    {
        $getId = Admin::where('id' , Auth::guard('admin')->user()->id)->first();
        return view('admin.profile.show_details' , compact(['getId']));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($adminDetail)
    {
        $detail = Admin::where('id' , $adminDetail)->first();
//        dd($detail);
        return view('admin.profile.edit_details')->with(compact('detail'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateAdminDetailRequest $request, AdminDetail $adminDetail)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(AdminDetail $adminDetail)
    {
        //
    }
}
