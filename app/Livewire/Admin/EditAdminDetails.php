<?php

namespace App\Livewire\Admin;

use Livewire\Component;

class EditAdminDetails extends Component
{
    public function render()
    {
        return view('livewire.admin.edit-admin-details');
    }
}
