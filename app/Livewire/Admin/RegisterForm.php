<?php

namespace App\Livewire\Admin;

use Livewire\Component;

class RegisterForm extends Component
{
    public function render()
    {
        return view('livewire.admin.register-form');
    }
}
