<?php

namespace App\Livewire\Admin;

use Livewire\Component;

class AdminSearch extends Component
{
    public function render()
    {
        return view('livewire.admin.admin-search');
    }
}
