<?php

namespace App\Livewire\Admin\admin;

use App\Models\admin\Admin;
use Illuminate\Support\Facades\Request;
use Livewire\Attributes\Rule;
use Livewire\Component;
use Livewire\WithPagination;
use function view;

class index extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $search='';
    public $editId;
    #[Rule('required|max:30|min:3')]
    public $name;
    #[Rule('required|email|max:30|unique:admins')]
    public $email;
    #[Rule('required|min:6')]
    public $password;
    #[Rule('required|min:6')]
    public $title;
//    #[Rule('required')]
    public $state;
    public $type;
    #[Rule('required')]
    public $phone;
    public $page = '';


        public function editForm($admin)
        {
            $admin = Admin::find($admin);
            $this->editId = $admin->id;
            $this->name = $admin->name;
            $this->email = $admin->email;
            $this->title = $admin->title;
            $this->type = $admin->type;
            $this->phone = $admin->phone;
            $this->state = $admin->state;
            return view('livewire.admin.admin.index');
        }
        public function cancelEdit()
        {
            $this->reset('editId' , 'name' , 'email' , 'title' , 'type' , 'phone' , 'status');
        }
        public function update()
                {
            //        $this->validate();
                    $admin = Admin::find($this->editId);
                    $admin->update(
                        [
                            'name' => $this->name,
                            'email' => $this->email,
                            'phone' => $this->phone,
                            'type' => $this->type,
                            'status' => $this->state,
                            'title' => $this->title,
                        ]
                    );

                    request()->session()->flash('status' , 'تم تحديث البيانات');
                    return $this->redirect('/admin/admins');


                }
        public function delete($id){
                    try {
                        Admin::findOrFail($id)->delete();
                    }
                    catch (\Exception $exception)
                    {
                        request()->session()->flash('status' , 'تم الحذف');
                        return;
                    }
            }
        public function toggle($id)
            {
                $checked = Admin::find($id);
                $checked->state = !$checked->state;
                $checked->save();
            }

        public function render()
            {
                return view('livewire.admin.admin.index' , [
                    'admins' => Admin::where('name' , 'like' , '%' . $this->search . '%')->orderBy('id','asc')->paginate(5),
                ]);
            }
}
